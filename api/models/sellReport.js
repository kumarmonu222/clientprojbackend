var mongoose = require('mongoose');

const SellReportSchema = new mongoose.Schema({
    shopId: { type: String, required: true },
    itemRecieved: [{
      sno: {type:Number},
      merchant: {type:String},
      city: {type:String},
      truckNo: {type:String},
      description: {type:String},
      nagsRecieved: {type:Number},
      nagsRecievedOutOf: {type:Number},
    }],
    itemSold: [{
      sno: {type:Number},
      retailer: {type:String},
      address: {type:String},
      description: {type:String},
      nagsSold: {type:Number},
    }]
}, {timestamps: true});

module.exports = mongoose.model('SellReport', SellReportSchema);