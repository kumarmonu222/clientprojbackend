var mongoose = require('mongoose');

// var ImageSchema = new mongoose.Schema({
//     imagePath  : {type: String, required: false},
// });

const BijakSchema = new mongoose.Schema({
    shopId: { type: String, required: true },
    no: {type: Number},
    city: {type: String},
    dateOfReciept: {type: Date},
    dispatchDate: {type: Date},
    ms: {type: String},
    truckNo: {type: String},
    itemRows: [{
      sno: {type:Number},
      description: {type:String},
      ctnKg: {type:Number},
      rate: {type:Number},
      itemAmount: {type: Number}
    }],
    commission:{type:Number},
    truckFreight: {type: Number},
    railFreight: {type: Number},
    stationMandiExp: {type: Number},
    unloading:{type: Number},
    postage: {type:Number},
    phone:{type:Number},
    coldStorage:{type:Number},
    loading:{type:Number},
    charity:{type:Number},
    totalGrossSale:{type:Number},
    amountInWord: {type:String},
    totalExpenses:{type:String},
    netSale:{type:String},
    totalNags: {type:Number}
}, {timestamps: true});

module.exports = mongoose.model('Bijak', BijakSchema);