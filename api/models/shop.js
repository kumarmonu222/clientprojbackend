const mongoose = require('mongoose');

const shopSchema = new mongoose.Schema({
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        required: true,
        ref: 'Users'
    },

    shopName: {
        type: String,
        required: false
    }

}, {timestamps: true});

module.exports = mongoose.model('Shop', shopSchema);