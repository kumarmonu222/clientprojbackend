const Bijak = require('../models/bijak');
var converter = require('number-to-words');

// exports.bijaks_get_all = (req, res, next) => {
//     Bijak.find(function(err, result){
//         if(err){
//             res.status(500).json({error: err});
//         } else {
//             if(result.length >= 1) {
//                 res.status(200).json({
//                     count: result.length,
//                     bijak: result
//                 });
//             } else {
//                 res.status(404).json({
//                     message: 'No Entries found'
//                 });
//             }            
//         }
//     }).populate('categoryLevel1 categoryLevel2 categoryLevel3 color size brand material price_range', 'name');
// }

exports.bijaks_get_bijak = (req, res, next) => {
    Bijak.findById(req.params.bijakId, function(err, result){
        if(err){
            res.status(500).json({error: err});
        } else {
            if(result){
                res.status(200).json({
                    bijak: result
                });
            } else {
                res.status(404).json({
                    message: 'No Entries found'
                });
            }            
        }
    });
}

exports.bijaks_add_bijak = async (req, res, next) => {
    try {
        const newBijak = new Bijak({ ...req.body });
        sum = 0; count=0;
        newBijak.itemRows.forEach(function(item){
            item.itemAmount = item.rate * item.ctnKg;
            sum += item.itemAmount;
            count += item.ctnKg;
        });
        newBijak.totalGrossSale = sum;
        newBijak.commission = sum * 0.07;
        newBijak.totalExpenses = newBijak.commission + newBijak.truckFreight + newBijak.railFreight 
        + newBijak.phone + newBijak.stationMandiExp + newBijak.unloading + newBijak.postage
        + newBijak.coldStorage + newBijak.loading + newBijak.charity;
        newBijak.netSale = newBijak.totalGrossSale - newBijak.totalExpenses;
        newBijak.totalNags = count;
        newBijak.amountInWord = converter.toWords(newBijak.netSale);

        const bijak_ = await newBijak.save();
        res.status(200).json({success: true, message: bijak_})
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
}

exports.bijaks_specific_shop_bijak = (req, res, next) => {
    Bijak.find({ shopId: req.params.shopId}, function(err, result){
        if(err){
            res.status(500).json({error: err});
        } else {
            if(result.length >= 1) {
                res.status(200).json({
                    count: result.length,
                    bijak: result
                });
            } else {
                res.status(404).json({
                    message: 'No Entries found'
                });
            }            
        }
    });
}

exports.bijaks_update_bijak = (req, res, next) => {

}

exports.bijaks_delete_bijak = async (req, res, next) => {
    try {
        const Bijak_ = await Bijak.findByIdAndDelete(req.params.bijakId);
        res.status(200).json({success: "bijak succesfully deleted", message: Bijak_})
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
}