const SellReport = require('../models/sellReport');
var converter = require('number-to-words');

// exports.sellReports_get_all = (req, res, next) => {
//     SellReport.find(function(err, result){
//         if(err){
//             res.status(500).json({error: err});
//         } else {
//             if(result.length >= 1) {
//                 res.status(200).json({
//                     count: result.length,
//                     sellReport: result
//                 });
//             } else {
//                 res.status(404).json({
//                     message: 'No Entries found'
//                 });
//             }            
//         }
//     }).populate('categoryLevel1 categoryLevel2 categoryLevel3 color size brand material price_range', 'name');
// }

exports.sellReports_get_sellReport = (req, res, next) => {
    SellReport.findById(req.params.sellReportId, function(err, result){
        if(err){
            res.status(500).json({error: err});
        } else {
            if(result){
                res.status(200).json({
                    sellReport: result
                });
            } else {
                res.status(404).json({
                    message: 'No Entries found'
                });
            }            
        }
    });
}

exports.sellReports_add_sellReport = async (req, res, next) => {
    try {
        const newSellReport = new SellReport({ ...req.body });
        // sum = 0; count=0;
        // newSellReport.itemRows.forEach(function(item){
        //     item.itemAmount = item.rate * item.ctnKg;
        //     sum += item.itemAmount;
        //     count += item.ctnKg;
        // });
        // newSellReport.totalGrossSale = sum;
        // newSellReport.commission = sum * 0.07;
        // newSellReport.totalExpenses = newSellReport.commission + newSellReport.truckFreight + newSellReport.railFreight 
        // + newSellReport.phone + newSellReport.stationMandiExp + newSellReport.unloading + newSellReport.postage
        // + newSellReport.coldStorage + newSellReport.loading + newSellReport.charity;
        // newSellReport.netSale = newSellReport.totalGrossSale - newSellReport.totalExpenses;
        // newSellReport.totalNags = count;
        // newSellReport.amountInWord = converter.toWords(newSellReport.netSale);
        const sellReport_ = await newSellReport.save();
        res.status(200).json({success: true, message: sellReport_})
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
}

exports.sellReports_specific_shop_sellReport = (req, res, next) => {
    SellReport.find({ shopId: req.params.shopId}, function(err, result){
        if(err){
            res.status(500).json({error: err});
        } else {
            if(result.length >= 1) {
                res.status(200).json({
                    count: result.length,
                    sellReport: result
                });
            } else {
                res.status(404).json({
                    message: 'No Entries found'
                });
            }            
        }
    });
}

exports.sellReports_update_sellReport = (req, res, next) => {

}

exports.sellReports_delete_sellReport = async (req, res, next) => {
    try {
        const SellReport_ = await SellReport.findByIdAndDelete(req.params.sellReportId);
        res.status(200).json({success: "sellReport succesfully deleted", message: SellReport_})
    } catch (error) {
        res.status(500).json({success: false, message: error.message});
    }
}