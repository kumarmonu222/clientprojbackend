const Shop = require('../models/shop');
const User = require('../models/user');

exports.allow_seller = async function (req, res) {
    try {
        const update = req.body;
        const id = req.params.id;
        const user = await User.findByIdAndUpdate(id, {$set: update}, {new: true});
        if(update.user_type === "seller") {
            const newShop = new Shop({ userId: req.params.id });
            newShop.save();
        } else {
            Shop.deleteOne({userId: req.params.id}, (err, result) => {
                if(err){
                    // res.status(500).json({error: err});
                } else {
                    // res.status(200).json({message: "successfully deleted"});
                }
            });   
        }
        res.status(200).json({ message: 'shop alloted'});
    } catch (error) {
        res.status(500).json({message: error.message});
    }
};

exports.get_seller_detail = async function (req, res) {
    try {
        const id = req.params.userId;
        const shop = await Shop.findOne({ userId: id }).populate('userId');
        if (!shop) return res.status(401).json({message: 'User does not exist'});
        res.status(200).json({shop});
    } catch (error) {
        res.status(500).json({message: error.message});
    }
};