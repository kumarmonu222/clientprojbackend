const express = require('express');
const {check} = require('express-validator');
const authenticate = require('../middlewares/authenticate');
const seller = require('../middlewares/seller');
const Bijak = require('../controllers/bijak');

const router = express.Router();

// router.get('/', Bijak.bijaks_get_all);
router.get('/specific-shop-bijaks/:shopId', Bijak.bijaks_specific_shop_bijak);
router.get('/:bijakId', Bijak.bijaks_get_bijak);
router.post('/', Bijak.bijaks_add_bijak);
router.put('/update/:bijakId', Bijak.bijaks_update_bijak);
router.delete('/delete/:bijakId', Bijak.bijaks_delete_bijak);

module.exports = router;