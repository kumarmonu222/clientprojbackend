const express = require('express');
const router = express.Router();

const User = require('../controllers/user');
const Users = require('../models/user');
const pagination = require('../middlewares/pagination');

router.get('/', pagination(Users), User.get_all_users); 
router.get('/:userId', User.index); 
router.put('/update', User.update);
router.post('/upload', User.upload);
router.post('/upload-bijak-image', User.upload_bijak_image);

module.exports = router;