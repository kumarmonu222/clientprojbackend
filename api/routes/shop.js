const express = require('express');
const Shop = require('../controllers/shop');
const authenticate = require('../middlewares/authenticate');
const admin = require('../middlewares/admin');
const seller = require('../middlewares/seller');
const router = express.Router();

router.put('/update/:id', authenticate, admin, Shop.allow_seller);
router.get('/detail/:userId', authenticate, seller, Shop.get_seller_detail);

module.exports = router;