const auth = require('./auth');
const user = require('./user');
const shop = require('./shop');
const bijak = require('./bijak');
const sellReport = require('./sellReport');
const authenticate = require('../middlewares/authenticate');
const seller = require('../middlewares/seller');
const admin = require('../middlewares/admin');

module.exports = app => {
    // General routes
    app.get('/', (req, res) => {
        res.status(200).send({ message: "Api is up"});
    });
    app.use('/api/auth', auth);
    
    // User routes
    app.use('/api/user', authenticate, user);

    // Seller routes
    app.use('/api/bijak', authenticate, seller, bijak);
    app.use('/api/sell-report', authenticate, seller, sellReport);

    // Admin routes
    app.use('/api/shop', shop);
    app.use('/api/users', authenticate, admin, user);
};