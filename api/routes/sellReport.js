const express = require('express');
const {check} = require('express-validator');
const authenticate = require('../middlewares/authenticate');
const seller = require('../middlewares/seller');
const SellReport = require('../controllers/sellReport');

const router = express.Router();

// router.get('/', SellReport.sellReports_get_all);
router.get('/specific-shop-sellReports/:shopId', SellReport.sellReports_specific_shop_sellReport);
router.get('/:sellReportId', SellReport.sellReports_get_sellReport);
router.post('/', SellReport.sellReports_add_sellReport);
router.put('/update/:sellReportId', SellReport.sellReports_update_sellReport);
router.delete('/delete/:sellReportId', SellReport.sellReports_delete_sellReport);

module.exports = router;