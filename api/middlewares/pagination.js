module.exports = function paginatedResults(model) {
    return async (req, res, next) => {
        const page = parseInt(req.query.page);
        const limit = parseInt(req.query.limit);
        const startIndex = (page - 1) * limit;
        const endIndex = page * limit;
        var nextPageUrl, previousPageUrl;
        const results = {};
        try {
            const modelData = await model.find().exec();
            if(endIndex < modelData.length) {
                nextPageUrl = `http://localhost:3000/api/users?page=${page + 1}&limit=${limit}`;
            }
            if(startIndex > 0) {
                previousPageUrl = `http://localhost:3000/api/users?page=${page - 1}&limit=${limit}`;
            }
            results.pagination = {
                page: page,
                limit: limit,
                total: modelData.length,
                nextPageUrl: nextPageUrl,
                previousPageUrl: previousPageUrl
            }
            results.data = await model.find().limit(limit).skip(startIndex).exec();
            res.paginatedResults = results;
            next();
        } catch (error) {
            res.status(500).json({ message: error.message });
        }
    }
}